package com.example.tmdbapp.data.rest

import com.example.tmdbapp.data.*
import com.example.tmdbapp.data.model.TMDBMovieDetail
import com.example.tmdbapp.data.model.TMDBMovieMediaResult
import com.example.tmdbapp.data.model.TMDBResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBService {

    @Headers(HEADER_CONTENT_TYPE)
    @GET(PATH_POPULAR)
    fun fetchPopularContent(@Query(QUERY_PAGE) page:Int): Single<TMDBResponse>

    @Headers(HEADER_CONTENT_TYPE)
    @GET(PATH_TOP_RATED)
    fun fetchTopRatedContent(@Query(QUERY_PAGE) page:Int): Single<TMDBResponse>

    @Headers(HEADER_CONTENT_TYPE)
    @GET(PATH_SEARCH)
    fun fetchSearchQueryContent(@Query(QUERY_PAGE) page: Int,
                                @Query(QUERY_SEARCH) textQuery:String): Single<TMDBResponse>

    @Headers(HEADER_CONTENT_TYPE)
    @GET(PATH_MOVIE_DETAIL)
    fun fetchMovieDetail(@Path(QUERY_MOVIE_ID) movieId:String):Single<TMDBMovieDetail>

    @Headers(HEADER_CONTENT_TYPE)
    @GET(PATH_MOVIE_MEDIA)
    fun fetchMovieMedia(@Path(QUERY_MOVIE_ID) movieId:String):Single<TMDBMovieMediaResult>


}