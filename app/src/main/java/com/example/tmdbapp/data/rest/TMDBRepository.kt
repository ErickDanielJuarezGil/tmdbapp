package com.example.tmdbapp.data.rest

import com.example.tmdbapp.data.model.TMDBMovieDetail
import com.example.tmdbapp.data.model.TMDBMovieMediaResult
import com.example.tmdbapp.data.model.TMDBResponse
import io.reactivex.Single
import javax.inject.Inject

class TMDBRepository @Inject constructor(private val repoService: TMDBService) {

    fun fetchPopularContent(page: Int): Single<TMDBResponse> {
        return repoService.fetchPopularContent(page)
    }

    fun fetchTopRated(page: Int): Single<TMDBResponse> {
        return repoService.fetchTopRatedContent(page)
    }

    fun fetchSearchQueryContent(textQuery: String, page: Int): Single<TMDBResponse> {
        return repoService.fetchSearchQueryContent(page, textQuery)
    }

    fun fetchMovieDetail(movieId: String): Single<TMDBMovieDetail> {
        return repoService.fetchMovieDetail(movieId)
    }

    fun fetchMovieMedia(movieId: String): Single<TMDBMovieMediaResult> {
        return repoService.fetchMovieMedia(movieId)
    }

}