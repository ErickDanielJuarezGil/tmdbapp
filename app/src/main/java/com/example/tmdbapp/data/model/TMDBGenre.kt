package com.example.tmdbapp.data.model

data class TMDBGenre (
    val id: Long,
    val name: String
)