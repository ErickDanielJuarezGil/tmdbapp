package com.example.tmdbapp.data.model

import com.google.gson.annotations.SerializedName

data class TMDBMovieDetail (
    val adult: Boolean,

    @SerializedName("backdrop_path")
    val backdropPath: String,

    val budget: Long,
    val genres: List<TMDBGenre>,
    val homepage: String,
    val id: Long,

    @SerializedName("imdb_id")
    val imdbID: String,

    @SerializedName("original_language")
    val originalLanguage: String,

    @SerializedName("original_title")
    val originalTitle: String,

    val overview: String,
    val popularity: Double,

    @SerializedName("poster_path")
    val posterPath: String? = "",

    @SerializedName("production_companies")
    val productionCompanies: List<TMDBProductionCompany>,

    @SerializedName("production_countries")
    val productionCountries: List<TMDBProductionCountry>,

    @SerializedName("release_date")
    val releaseDate: String,

    val revenue: Long,
    val runtime: Long,

    @SerializedName("spoken_languages")
    val spokenLanguages: List<TMDBSpokenLanguage>,

    val status: String,
    val tagline: String,
    val title: String,
    val video: Boolean,

    @SerializedName("vote_average")
    val voteAverage: Double,

    @SerializedName("vote_count")
    val voteCount: Long
)