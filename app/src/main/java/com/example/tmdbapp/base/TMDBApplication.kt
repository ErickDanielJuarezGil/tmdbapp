package com.example.tmdbapp.base

import com.example.tmdbapp.di.component.ApplicationComponent
import com.example.tmdbapp.di.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class TMDBApplication: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component: ApplicationComponent =
            DaggerApplicationComponent
                .builder()
                .application(this)
                .build()
        component.inject(this)

        return component
    }

}