package com.example.tmdbapp.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel<N> : ViewModel() {

    var navigator: N? = null

}