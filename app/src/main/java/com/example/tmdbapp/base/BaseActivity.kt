package com.example.tmdbapp.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tmdbapp.util.ViewModelFactory
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity<T : ViewDataBinding, V : ViewModel>: DaggerAppCompatActivity() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory

    /*
    * Class variables
    * */
    lateinit var mBinding:T

    /*
    * Remote variables
    * */
    lateinit var viewModel: V
    abstract val bindingVariable: Int
    @get:LayoutRes
    abstract val layoutId: Int

    protected abstract fun getViewModel(): Class<V>

    protected abstract fun setupNavigator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(getViewModel())
        setupNavigator()
        performDataBinding()
    }

    fun Context.launchActivity(clazz: Class<out Activity>) = startActivity(Intent(this, clazz))

    private fun performDataBinding(){
        mBinding = DataBindingUtil.setContentView(this, layoutId)
        mBinding.setVariable(bindingVariable, viewModel)
        mBinding.executePendingBindings()
    }

}