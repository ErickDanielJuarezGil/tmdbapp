package com.example.tmdbapp.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tmdbapp.util.ViewModelFactory
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<T : ViewDataBinding, V : ViewModel>: DaggerFragment() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory

    /*
    * Class variables
    * */
    lateinit var mBinding:T

    /*
    * Remote variables
    * */
    lateinit var viewModel: V
    abstract val bindingVariable: Int
    @get:LayoutRes
    abstract val layoutId: Int

    protected abstract fun getViewModel(): Class<V>

    protected abstract fun setupNavigator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProvider(this, viewModelFactory).get(getViewModel())
        setupNavigator()
        performDataBinding(inflater, container)
        return mBinding.root
    }

    fun Context.launchActivity(clazz: Class<out Activity>) = startActivity(Intent(requireContext(), clazz))

    private fun performDataBinding(inflater: LayoutInflater, container: ViewGroup?){
        mBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        mBinding.setVariable(bindingVariable, viewModel)
        mBinding.executePendingBindings()
    }

}