package com.example.tmdbapp.util

const val APP_TAG = "TMDBApp"
const val POPULAR = "popular"
const val TOP_RATED = "top_rated"
const val TMDB_VIDEO_KEY = "TMDB_VIDEO_KEY"
const val TMDB_VIDEO_SUGGESTION = "TMDB_VIDEO_SUGGESTION"