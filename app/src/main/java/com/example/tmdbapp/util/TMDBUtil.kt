package com.example.tmdbapp.util

import android.app.Activity
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.tmdbapp.BuildConfig
import com.example.tmdbapp.data.PATH_IMAGES
import com.example.tmdbapp.ui.homeActivity.HomeActivity

fun setupStartRange(voteAverage:Double, mLottieRatingAnimation: LottieAnimationView):LottieAnimationView {
        when {
            voteAverage < 1 -> {
                mLottieRatingAnimation.setMaxFrame(0)
            }
            voteAverage in 1.0..1.9 -> {
                mLottieRatingAnimation.setMaxFrame(14)//One star
            }
            voteAverage in 2.0..2.9 -> {
                mLottieRatingAnimation.setMaxFrame(29)//Two stars
            }
            voteAverage in 3.0..3.9 -> {
                mLottieRatingAnimation.setMaxFrame(44)//Three stars
            }
            voteAverage in 4.0..4.9 -> {
                mLottieRatingAnimation.setMaxFrame(59)//Four stars
            }
            else -> {
                mLottieRatingAnimation.setMaxFrame(77)//Five stars
            }
        }
        return mLottieRatingAnimation
    }

/*
     * 5 number of stars
     * 10 fix
     */
fun getFixedVoteAverage(voteAverage: Double) = voteAverage * 5 / 10

fun moveImageWithPlaceholder(posterPath: String, movieImage: ImageView, moviePlaceHolder:LottieAnimationView) =
    Glide.with(movieImage)
        .load(BuildConfig.BASE_URL_IMAGES.plus(PATH_IMAGES).plus(posterPath))
        .addListener(getImageLoadingListener(moviePlaceHolder))
        .into(movieImage)

private fun getImageLoadingListener(moviePlaceHolder:LottieAnimationView):RequestListener<Drawable> = object: RequestListener<Drawable> {
    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?,
                              isFirstResource: Boolean): Boolean {
        return false;
    }

    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?,
                                 dataSource: DataSource?, isFirstResource: Boolean, ): Boolean {
        moviePlaceHolder.pauseAnimation()
        moviePlaceHolder.visibility = View.GONE
        return false
    }
}

fun fixHomeStatusBar(activity:Activity, fullScreenEnabled:Boolean){
    val mHomeActivity = (activity as HomeActivity)
    mHomeActivity.fullScreenEnabled = fullScreenEnabled
    mHomeActivity.fixStatusBar()
}