package com.example.tmdbapp.ui.detailFragment

import android.util.Log
import com.example.tmdbapp.base.BaseViewModel
import com.example.tmdbapp.data.model.TMDBMovieDetail
import com.example.tmdbapp.data.model.TMDBMovieMediaResult
import com.example.tmdbapp.data.rest.TMDBRepository
import com.example.tmdbapp.util.APP_TAG
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailFragmentViewModel @Inject constructor(val repository: TMDBRepository) :
    BaseViewModel<DetailFragmentNavigator>() {

    private val comDisp by lazy { CompositeDisposable() }

    fun fetchMovieDetails(movieId:String){
        comDisp.add(
            repository.fetchMovieDetail(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { navigator?.showLoading() }
                .doFinally { navigator?.hideLoading() }
                .subscribeWith(object : DisposableSingleObserver<TMDBMovieDetail>() {
                    override fun onSuccess(tmdbResponse: TMDBMovieDetail) {
                        navigator?.fillMovieData(tmdbResponse)
                    }

                    override fun onError(e: Throwable) {
                        Log.e(APP_TAG, e.localizedMessage.orEmpty())
                        navigator?.showFetchDetailError()
                    }

                })
        )
    }

    fun requestMovieMedia(movieId:String){
        comDisp.add(
            repository.fetchMovieMedia(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { navigator?.showLoading() }
                .doFinally { navigator?.hideLoading() }
                .subscribeWith(object : DisposableSingleObserver<TMDBMovieMediaResult>() {
                    override fun onSuccess(tmdbResponse: TMDBMovieMediaResult) {
                        navigator?.prepareMovieMedia(tmdbResponse)
                    }

                    override fun onError(e: Throwable) {
                        Log.e(APP_TAG, e.localizedMessage.orEmpty())
                        navigator?.showMovieMediaError()
                    }

                })
        )
    }

}