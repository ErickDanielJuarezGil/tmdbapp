package com.example.tmdbapp.ui.youtubePlayer

import androidx.databinding.DataBindingUtil
import com.example.tmdbapp.BuildConfig.YOUTUBE_KEY
import com.example.tmdbapp.R
import com.example.tmdbapp.databinding.TmdbYoutubePlayerBinding
import com.example.tmdbapp.util.TMDB_VIDEO_KEY
import com.example.tmdbapp.util.TMDB_VIDEO_SUGGESTION
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer

class TMDBYoutubePlayer:YouTubeBaseActivity() {

    lateinit var mBinding:TmdbYoutubePlayerBinding

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        mBinding = DataBindingUtil.setContentView(this, R.layout.tmdb_youtube_player)
        val mVideoKey = intent.extras?.getString(TMDB_VIDEO_KEY)
        val mVideoName = intent.extras?.getString(TMDB_VIDEO_SUGGESTION)
        mBinding.movieTitle = mVideoName
        mBinding.executePendingBindings()
        mBinding.youtubePlayer.initialize(YOUTUBE_KEY, object: YouTubePlayer.OnInitializedListener{
            override fun onInitializationSuccess(
                youtubePlayerProvider: YouTubePlayer.Provider?,
                youtubePlayer: YouTubePlayer?,
                wasRestored: Boolean) {
                youtubePlayer?.cueVideo(mVideoKey)
            }

            override fun onInitializationFailure(
                youtubePlayerProvider: YouTubePlayer.Provider?,
                youtubePlayerResult: YouTubeInitializationResult?) {
                print("Error")
            }
        })
        mBinding.closeButton.setOnClickListener { finish() }
    }

}