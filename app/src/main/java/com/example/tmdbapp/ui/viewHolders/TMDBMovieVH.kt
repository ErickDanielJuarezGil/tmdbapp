package com.example.tmdbapp.ui.viewHolders

import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdbapp.R
import com.example.tmdbapp.data.model.TMDBMovie
import com.example.tmdbapp.databinding.RowMovieBinding
import com.example.tmdbapp.ui.adapters.TMDBMoviesAdapter.ItemClick
import com.example.tmdbapp.util.getFixedVoteAverage
import com.example.tmdbapp.util.moveImageWithPlaceholder
import com.example.tmdbapp.util.setupStartRange

class TMDBMovieVH constructor(var viewBinding: RowMovieBinding, var mOnClickListener: ItemClick):
    RecyclerView.ViewHolder(viewBinding.root) {

    fun bind(movie: TMDBMovie) = with(movie) {
        viewBinding.movieData = this
        viewBinding.executePendingBindings()
        viewBinding.rootCard.setOnClickListener {
            mOnClickListener.onClick(this)
            ViewCompat.setTransitionName(it, viewBinding.root.context.getString(R.string.movie_image))
        }
        if(posterPath.orEmpty().isNotEmpty()){
            moveImageWithPlaceholder(posterPath.orEmpty(), viewBinding.movieImage, viewBinding.moviePlaceHolder)
        } else {
            showNoImgPlaceHolder()
        }
        setupStartRange(getFixedVoteAverage(voteAverage),
            viewBinding.mLottieRatingAnimation).playAnimation()
    }

    private fun showNoImgPlaceHolder(){

    }

}