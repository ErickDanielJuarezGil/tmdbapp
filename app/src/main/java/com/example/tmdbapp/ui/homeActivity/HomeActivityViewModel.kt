package com.example.tmdbapp.ui.homeActivity

import com.example.tmdbapp.base.BaseViewModel
import javax.inject.Inject

class HomeActivityViewModel @Inject constructor() :
    BaseViewModel<HomeActivityNavigator>() {
}