package com.example.tmdbapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdbapp.data.model.TMDBMovie
import com.example.tmdbapp.databinding.RowMovieBinding
import com.example.tmdbapp.ui.viewHolders.TMDBMovieVH


class TMDBMoviesAdapter constructor(var dataSet: Array<TMDBMovie>, var mOnClickListener: ItemClick):
    RecyclerView.Adapter<TMDBMovieVH>() {

    override fun onBindViewHolder(holder: TMDBMovieVH, position: Int) {
        holder.bind(dataSet.get(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TMDBMovieVH {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowMovieBinding.inflate(layoutInflater, parent, false)
        return TMDBMovieVH(itemBinding, mOnClickListener)
    }

    override fun onViewAttachedToWindow(holder: TMDBMovieVH) {
        super.onViewAttachedToWindow(holder)
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 350
        holder.viewBinding.root.startAnimation(anim)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    interface ItemClick {

        fun onClick(tmdbMovie: TMDBMovie)

    }

}
