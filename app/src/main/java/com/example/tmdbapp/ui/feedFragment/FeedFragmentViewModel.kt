package com.example.tmdbapp.ui.feedFragment

import android.util.Log
import com.example.tmdbapp.base.BaseViewModel
import com.example.tmdbapp.data.model.TMDBResponse
import com.example.tmdbapp.data.rest.TMDBRepository
import com.example.tmdbapp.util.APP_TAG
import com.example.tmdbapp.util.POPULAR
import com.example.tmdbapp.util.TOP_RATED
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FeedFragmentViewModel @Inject constructor(val repository: TMDBRepository) :
    BaseViewModel<FeedFragmentNavigator>() {

    private val comDisp by lazy { CompositeDisposable() }

    fun fetchContent(textQuery: String, page:Int, isInitial:Boolean) {
        val repositoryContent = getRepositoryService(textQuery, page)
        comDisp.add(
            repositoryContent
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { navigator?.showLoading() }
                .doFinally { navigator?.hideLoading() }
                .subscribeWith(object : DisposableSingleObserver<TMDBResponse>() {
                    override fun onSuccess(tmdbResponse: TMDBResponse) {
                        if(isInitial) {
                            navigator?.fillFetchedContent(tmdbResponse)
                        } else {
                            navigator?.addNewImages(tmdbResponse)
                        }
                    }

                    override fun onError(e: Throwable) {
                        Log.e(APP_TAG, e.localizedMessage.orEmpty())
                        navigator?.showFetchContentError()
                    }

                })
        )
    }

    private fun getRepositoryService(textQuery: String, page:Int) = when (textQuery) {
        POPULAR -> {
            repository.fetchPopularContent(page)
        }
        TOP_RATED -> {
            repository.fetchTopRated(page)
        } else -> {
            repository.fetchSearchQueryContent(textQuery, page)
        }
    }

}