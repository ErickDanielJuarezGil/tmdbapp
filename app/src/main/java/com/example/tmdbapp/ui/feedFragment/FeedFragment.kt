package com.example.tmdbapp.ui.feedFragment

import android.animation.Animator
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewAnimationUtils
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tmdbapp.BR
import com.example.tmdbapp.R
import com.example.tmdbapp.base.BaseFragment
import com.example.tmdbapp.data.isInternetAvailable
import com.example.tmdbapp.data.model.TMDBMovie
import com.example.tmdbapp.data.model.TMDBResponse
import com.example.tmdbapp.databinding.FeedFragmentBinding
import com.example.tmdbapp.ui.adapters.TMDBMoviesAdapter
import com.example.tmdbapp.ui.adapters.TMDBMoviesAdapter.ItemClick
import com.example.tmdbapp.util.POPULAR
import com.example.tmdbapp.util.TOP_RATED
import com.example.tmdbapp.util.fixHomeStatusBar
import javax.inject.Inject
import kotlin.math.hypot
import kotlin.math.max


class FeedFragment @Inject constructor() :
    BaseFragment<FeedFragmentBinding, FeedFragmentViewModel>(), FeedFragmentNavigator {

    private var currentPage = 1
    private var isOpen = false
    var mCurrentTextQuery = ""
    private fun getNextPage(): Int {
        currentPage += 1
        return currentPage
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initFeed()
    }

    override fun onResume() {
        super.onResume()
        fixHomeStatusBar(requireActivity(), false)
    }

    private fun initFeed() {
        mCurrentTextQuery = POPULAR
        fetchContent(true)
        setupButtons()
    }

    private fun setupButtons() {
        mBinding.popularBtn.setOnClickListener { popularButtonAction() }
        mBinding.topRatedBtn.setOnClickListener { topRattedButtonAction() }
        mBinding.floatingSearchBtn.setOnClickListener { startFloatingAnimation() }
        mBinding.btnSearch.setOnClickListener { searchByQuery() }
        mBinding.searchEditText.setOnEditorActionListener { _, _, _ ->
            searchByQuery()
            true
        }
    }

    private fun searchByQuery(){
        val mQuery = mBinding.searchEditText.text.toString()
        if(mQuery.trim().isNotEmpty()) {
            TextViewCompat.setTextAppearance(mBinding.popularBtn,
                R.style.Theme_Tmdbapp_FakeTabUnselected)
            TextViewCompat.setTextAppearance(
                mBinding.topRatedBtn,
                R.style.Theme_Tmdbapp_FakeTabUnselected)
            currentPage = 1
            mCurrentTextQuery = mQuery
            fetchContent(true)
            startFloatingAnimation()
        }
    }

    override fun showLoading() {
        mBinding.mLotieLoadingContent.visibility = VISIBLE
    }

    override fun hideLoading() {
        mBinding.mLotieLoadingContent.visibility = GONE
    }

    private fun startFloatingAnimation(){
        if (!isOpen) {
            mBinding.floatingSearchBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_close))
            val x: Int = mBinding.secondaryContent.right
            val y: Int = mBinding.secondaryContent.bottom
            val startRadius = 0
            val endRadius = hypot(
                mBinding.rootContainer.width.toDouble(),
                mBinding.rootContainer.height.toDouble()).toInt()
            val anim: Animator = ViewAnimationUtils.createCircularReveal(
                mBinding.secondaryContent, x, y, startRadius.toFloat(), endRadius.toFloat())
            mBinding.secondaryContent.visibility = VISIBLE
            anim.start()
            isOpen = true
        } else {
            mBinding.floatingSearchBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_search))
            val x: Int = mBinding.secondaryContent.right
            val y: Int = mBinding.secondaryContent.bottom
            val startRadius: Int = max(mBinding.secondaryContent.width,
                mBinding.secondaryContent.height)
            val endRadius = 0
            val anim: Animator = ViewAnimationUtils.createCircularReveal(
                mBinding.secondaryContent,
                x, y, startRadius.toFloat(), endRadius.toFloat())
            anim.addListener(animatorListener)
            anim.start()
            isOpen = false
        }
    }

    private val animatorListener = object : Animator.AnimatorListener {

        override fun onAnimationStart(p0: Animator?) {}

        override fun onAnimationEnd(p0: Animator?) {
            mBinding.secondaryContent.visibility = GONE
        }

        override fun onAnimationCancel(p0: Animator?) {}

        override fun onAnimationRepeat(p0: Animator?) {}

    }

    private fun popularButtonAction() {
        currentPage = 1
        mCurrentTextQuery = POPULAR
        fetchContent(true)
        TextViewCompat.setTextAppearance(mBinding.popularBtn,
            R.style.Theme_Tmdbapp_FakeTabSelected)
        TextViewCompat.setTextAppearance(
            mBinding.topRatedBtn,
            R.style.Theme_Tmdbapp_FakeTabUnselected)
    }

    private fun topRattedButtonAction() {
        currentPage = 1
        mCurrentTextQuery = TOP_RATED
        fetchContent(true)
        TextViewCompat.setTextAppearance(
            mBinding.popularBtn,
            R.style.Theme_Tmdbapp_FakeTabUnselected)
        TextViewCompat.setTextAppearance(
            mBinding.topRatedBtn,
            R.style.Theme_Tmdbapp_FakeTabSelected)
    }

    override fun fillFetchedContent(tmdbResponse: TMDBResponse) {
        with(tmdbResponse) {
            if (totalResults == 0) {
                showEmptyMessage()
            } else {
                mBinding.mLotieNoContent.visibility = GONE
                mBinding.recyclerMovies.visibility = VISIBLE
                fillAndPrepareRecyclerView(tmdbResponse.results)
            }
            fillTotalResults(totalResults)
        }
    }

    private fun showEmptyMessage() {
        mBinding.recyclerMovies.visibility = GONE
        mBinding.mLotieNoContent.visibility = VISIBLE
    }

    private fun fillAndPrepareRecyclerView(results: Array<TMDBMovie>) {
        mBinding.recyclerMovies.visibility = VISIBLE
        mBinding.recyclerMovies.adapter = TMDBMoviesAdapter(results, mOnClickListener)
        mBinding.recyclerMovies.layoutManager = LinearLayoutManager(context)
        setupScroll()
    }

    private fun setupScroll() {
        mBinding.recyclerMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    fetchContent(false)
                }
            }
        })
    }

    override fun addNewImages(tmdbResponse: TMDBResponse) {
        val mAdapter = mBinding.recyclerMovies.adapter as TMDBMoviesAdapter
        mAdapter.dataSet = mAdapter.dataSet.plus(tmdbResponse.results)
        mAdapter.notifyDataSetChanged()
        mBinding.recyclerMovies.scheduleLayoutAnimation()
        fillTotalResults(tmdbResponse.totalResults)
    }

    private var mOnClickListener: ItemClick = object : ItemClick {
        override fun onClick(tmdbMovie: TMDBMovie) {
            val bundle = Bundle()
            bundle.putString("movieId", tmdbMovie.id)
            requireView()
                .findNavController()
                .navigate(FeedFragmentDirections.actionFeedFragmentToDetailFragment().actionId, bundle)
        }
    }

    private fun fillTotalResults(totalResults: Int) {
        var mAdapter:TMDBMoviesAdapter? = null
        if (mBinding.recyclerMovies.adapter != null){
            mAdapter = mBinding.recyclerMovies.adapter as TMDBMoviesAdapter
        }
        mBinding.totalResult = getString(R.string.total_result).plus(mAdapter?.dataSet?.size?.or(0))
            .plus("/").plus(totalResults.toString())
        mBinding.executePendingBindings()
    }

    private fun fetchContent(isInitial:Boolean){
        synchronized(this){
            if(isInternetAvailable(requireContext())) {
                if(!isInitial){
                    currentPage = getNextPage()
                }
                mBinding.mLotieNoContent.visibility = GONE
                mBinding.recyclerMovies.visibility = VISIBLE
                mBinding.mLottieSearchAnimation.visibility = VISIBLE
                viewModel.fetchContent(mCurrentTextQuery, currentPage, isInitial)
            } else {
                mBinding.mLotieNoContent.visibility = VISIBLE
                mBinding.recyclerMovies.visibility = GONE
                if(isOpen){
                    startFloatingAnimation()
                    mBinding.mLottieSearchAnimation.visibility = GONE
                }
            }
        }
    }

    override fun showFetchContentError() {
        mBinding.mLotieNoContent.visibility = VISIBLE
    }

    override fun setupNavigator() {
        viewModel.navigator = this
    }

    override fun getViewModel() = FeedFragmentViewModel::class.java

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.feed_fragment

}