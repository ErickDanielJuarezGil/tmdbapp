package com.example.tmdbapp.ui.detailFragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.example.tmdbapp.BR
import com.example.tmdbapp.BuildConfig
import com.example.tmdbapp.R
import com.example.tmdbapp.base.BaseFragment
import com.example.tmdbapp.data.PATH_IMAGES
import com.example.tmdbapp.data.isInternetAvailable
import com.example.tmdbapp.data.model.TMDBMovieDetail
import com.example.tmdbapp.data.model.TMDBMovieMediaResult
import com.example.tmdbapp.data.model.TMDBProductionCompany
import com.example.tmdbapp.databinding.DetailFragmentBinding
import com.example.tmdbapp.ui.youtubePlayer.TMDBYoutubePlayer
import com.example.tmdbapp.util.*
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import javax.inject.Inject

class DetailFragment @Inject constructor() :
    BaseFragment<DetailFragmentBinding, DetailFragmentViewModel>(), DetailFragmentNavigator {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchMovieDetails()
        mBinding.backButton.setOnClickListener { requireActivity().onBackPressed() }
    }

    override fun onResume() {
        super.onResume()
        fixHomeStatusBar(requireActivity(), true)
    }

    private fun fetchMovieDetails() {
        if (isInternetAvailable(requireContext())) {
            viewModel.fetchMovieDetails(arguments?.getString("movieId").orEmpty())
        } else {
            showNoInternetConnectionMsg()
        }
    }

    override fun fillMovieData(tmdbResponse: TMDBMovieDetail) {
        with(tmdbResponse) {
            setupStartRange(
                getFixedVoteAverage(voteAverage),
                mBinding.mLottieRatingAnimation).playAnimation()
            mBinding.tmdbMovieDetail = tmdbResponse
            if (posterPath.orEmpty().isNotEmpty()) {
                mBinding.movieImage.scaleType = ImageView.ScaleType.CENTER_CROP
                moveImageWithPlaceholder(
                    tmdbResponse.posterPath.orEmpty(),
                    mBinding.movieImage,
                    mBinding.moviePlaceHolder)
            } else {
                showNoImgPlaceHolder()
            }
            fillProductionCompanies(productionCompanies)
            setupPlayButton(imdbID)
        }
    }

    private fun setupPlayButton(movieId:String){
        mBinding.playMediaButton.setOnClickListener {
            viewModel.requestMovieMedia(movieId)
        }
    }

    private fun fillProductionCompanies(productionCompanies: List<TMDBProductionCompany>) {
        val mFilteredCompanies = productionCompanies.filter { !it.logoPath.isNullOrBlank() }
        mFilteredCompanies.forEach {
            val mCompanyImg = ImageView(requireContext())
            val mContainerHeight = mBinding.scrollCompanies.height
            val mRelativeLayotContainer = RelativeLayout(requireContext())
            Glide.with(this)
                .asBitmap()
                .circleCrop()
                .centerInside()
                .load(BuildConfig.BASE_URL_IMAGES.plus(PATH_IMAGES).plus(it.logoPath))
                .into(mCompanyImg)
            mCompanyImg.layoutParams = LinearLayout.LayoutParams(mContainerHeight, mContainerHeight, 1.0f)
            val mBgCompanyImg = ImageView(requireContext())
            Glide.with(this)
                .asBitmap()
                .circleCrop()
                .load(BuildConfig.URL_BG_PRODUCTION_COMPANIES)
                .into(mBgCompanyImg)
            mBgCompanyImg.layoutParams = LinearLayout.LayoutParams(mContainerHeight, mContainerHeight, 1.0f)
            mRelativeLayotContainer.addView(mBgCompanyImg)
            mRelativeLayotContainer.addView(mCompanyImg)
            (mBgCompanyImg.layoutParams as ViewGroup.MarginLayoutParams).rightMargin = 30
            (mCompanyImg.layoutParams as ViewGroup.MarginLayoutParams).rightMargin = 30
            mBinding.productionCompaniesImages.addView(mRelativeLayotContainer)
        }
    }

    override fun prepareMovieMedia(tmdbResponse: TMDBMovieMediaResult) {
        if(tmdbResponse.results.isNotEmpty()){
            val mTrailer = tmdbResponse.results.first()
            val mYoutubePlayerIntent = Intent(requireActivity(), TMDBYoutubePlayer::class.java)
            val mExtras = Bundle()
            mExtras.putString(TMDB_VIDEO_KEY, mTrailer.key.orEmpty())
            mExtras.putString(TMDB_VIDEO_SUGGESTION, mBinding.movieTitle.text.toString())
            mYoutubePlayerIntent.putExtras(mExtras)
            startActivity(mYoutubePlayerIntent)
        } else {
            showNoMediaMessage()
        }
    }

    private fun showNoMediaMessage(){

    }

    override fun showMovieMediaError() {

    }

    private fun showNoImgPlaceHolder() {

    }

    override fun showFetchDetailError() {

    }

    private fun showNoInternetConnectionMsg() {

    }

    override fun showLoading() {
        mBinding.mLotieLoadingContent.visibility = VISIBLE
    }

    override fun hideLoading() {
        mBinding.mLotieLoadingContent.visibility = GONE
    }

    override fun setupNavigator() {
        viewModel.navigator = this
    }

    override fun getViewModel() = DetailFragmentViewModel::class.java

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.detail_fragment

}