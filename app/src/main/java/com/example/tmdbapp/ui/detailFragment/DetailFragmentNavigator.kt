package com.example.tmdbapp.ui.detailFragment

import com.example.tmdbapp.data.model.TMDBMovieDetail
import com.example.tmdbapp.data.model.TMDBMovieMediaResult

interface DetailFragmentNavigator {
    fun showLoading()
    fun hideLoading()
    fun fillMovieData(tmdbResponse: TMDBMovieDetail)
    fun showFetchDetailError()
    fun prepareMovieMedia(tmdbResponse: TMDBMovieMediaResult)
    fun showMovieMediaError()
}