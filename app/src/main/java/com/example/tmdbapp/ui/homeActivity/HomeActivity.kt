package com.example.tmdbapp.ui.homeActivity

import android.content.res.Resources
import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.tmdbapp.BR
import com.example.tmdbapp.R
import com.example.tmdbapp.base.BaseActivity
import com.example.tmdbapp.databinding.HomeActivityBinding
import javax.inject.Inject

class HomeActivity @Inject constructor() :
    BaseActivity<HomeActivityBinding, HomeActivityViewModel>(), HomeActivityNavigator {

    var fullScreenEnabled:Boolean = true

    override fun setupNavigator() {
        viewModel.navigator = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fixStatusBar()
    }

    fun fixStatusBar() =
        with(mBinding.navHomeFragment.layoutParams as ConstraintLayout.LayoutParams) {
            if(fullScreenEnabled) {
                topMargin = 0
            } else {
                topMargin = statusBarHeight(resources)
            }
            mBinding.executePendingBindings()
        }


    private fun statusBarHeight(res: Resources): Int {
        return (24 * res.displayMetrics.density).toInt()
    }

    override fun getViewModel() = HomeActivityViewModel::class.java

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.home_activity

}