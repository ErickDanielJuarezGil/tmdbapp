package com.example.tmdbapp.ui.splashView

import com.example.tmdbapp.base.BaseViewModel
import com.example.tmdbapp.data.rest.TMDBRepository
import javax.inject.Inject

class SplashActivityViewModel @Inject constructor(val repository: TMDBRepository) :
    BaseViewModel<SplashActivityNavigator>() {

}