package com.example.tmdbapp.ui.splashView

import android.animation.Animator
import android.os.Bundle
import com.example.tmdbapp.BR
import com.example.tmdbapp.R
import com.example.tmdbapp.base.BaseActivity
import com.example.tmdbapp.databinding.SplashActivityBinding
import com.example.tmdbapp.ui.homeActivity.HomeActivity
import javax.inject.Inject

class SplashActivity @Inject constructor() :
    BaseActivity<SplashActivityBinding, SplashActivityViewModel>(), SplashActivityNavigator {

    override fun setupNavigator() {
        viewModel.navigator = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        mBinding.mLottieSplashAnimation.addAnimatorListener(object: Animator.AnimatorListener{
            override fun onAnimationStart(p0: Animator?) {
                //Empty implementation
            }

            override fun onAnimationEnd(p0: Animator?) {
                launchActivity(HomeActivity::class.java)
                finish()
            }

            override fun onAnimationCancel(p0: Animator?) {
                //Empty implementation
            }

            override fun onAnimationRepeat(p0: Animator?) {
                //Empty implementation
            }

        })
    }

    override fun getViewModel() = SplashActivityViewModel::class.java

    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.splash_activity

}