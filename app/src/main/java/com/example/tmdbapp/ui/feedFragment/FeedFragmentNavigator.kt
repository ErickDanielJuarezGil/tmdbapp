package com.example.tmdbapp.ui.feedFragment

import com.example.tmdbapp.data.model.TMDBResponse

interface FeedFragmentNavigator {
    fun fillFetchedContent(tmdbResponse: TMDBResponse)
    fun showFetchContentError()
    fun addNewImages(tmdbResponse: TMDBResponse)
    fun showLoading()
    fun hideLoading()
}