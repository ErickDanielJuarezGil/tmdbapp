package com.example.tmdbapp.di.module

import androidx.lifecycle.ViewModel
import com.example.tmdbapp.di.util.ViewModelKey
import com.example.tmdbapp.ui.detailFragment.DetailFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailFragmentModule {
    @Binds
    @IntoMap
    @ViewModelKey(DetailFragmentViewModel::class)
    abstract fun bindDetailFragmentModule(mainViewModel: DetailFragmentViewModel): ViewModel
}