package com.example.tmdbapp.di.module

import androidx.lifecycle.ViewModel
import com.example.tmdbapp.di.util.ViewModelKey
import com.example.tmdbapp.ui.homeActivity.HomeActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class HomeActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeActivityViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: HomeActivityViewModel): ViewModel

}