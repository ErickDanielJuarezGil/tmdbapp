package com.example.tmdbapp.di.module

import com.example.tmdbapp.ui.detailFragment.DetailFragment
import com.example.tmdbapp.ui.feedFragment.FeedFragment
import com.example.tmdbapp.ui.homeActivity.HomeActivity
import com.example.tmdbapp.ui.splashView.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewBindingModule {

    @ContributesAndroidInjector(modules = [SplashActivityModule::class])
    internal abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    internal abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [FeedFragmentModule::class])
    internal abstract fun bindFeedFragment(): FeedFragment

    @ContributesAndroidInjector(modules = [DetailFragmentModule::class])
    internal abstract fun bindDetailFragment(): DetailFragment

}