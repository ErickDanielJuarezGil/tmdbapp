package com.example.tmdbapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tmdbapp.di.util.ViewModelKey
import com.example.tmdbapp.ui.detailFragment.DetailFragmentViewModel
import com.example.tmdbapp.ui.feedFragment.FeedFragmentViewModel
import com.example.tmdbapp.ui.homeActivity.HomeActivityViewModel
import com.example.tmdbapp.ui.splashView.SplashActivityViewModel
import com.example.tmdbapp.util.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashActivityViewModel::class)
    abstract fun bindSplashViewViewModel(splashViewViewModel: SplashActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeActivityViewModel::class)
    abstract fun bindHomeViewViewModel(splashViewViewModel: HomeActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeedFragmentViewModel::class)
    abstract fun bindFeedFragmentModule(feedFragmentViewModule: FeedFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailFragmentViewModel::class)
    abstract fun bindDetailFragmentModule(detailFragmentViewModule: DetailFragmentViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}