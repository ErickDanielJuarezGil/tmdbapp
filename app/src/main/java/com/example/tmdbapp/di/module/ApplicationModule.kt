package com.example.tmdbapp.di.module

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.example.tmdbapp.BuildConfig.BASE_URL
import com.example.tmdbapp.data.rest.TMDBRepository
import com.example.tmdbapp.data.rest.TMDBService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule {

    @Singleton
    @Provides
    internal fun provideRetrofit(context: Context): Retrofit {
        val client =
            OkHttpClient
                .Builder()
                .addInterceptor(getChuckerInterceptor(context))
                .build()
        return Retrofit.Builder().baseUrl(BASE_URL)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getChuckerInterceptor(context: Context) = ChuckerInterceptor.Builder(context).build()

    @Singleton
    @Provides
    internal fun provideRetrofitService(retrofit: Retrofit): TMDBService {
        return retrofit.create(TMDBService::class.java)
    }

    @Singleton
    @Provides
    internal fun provideTMDBRepository(tmdbRepository: TMDBService):TMDBRepository {
        return TMDBRepository(tmdbRepository)
    }

}