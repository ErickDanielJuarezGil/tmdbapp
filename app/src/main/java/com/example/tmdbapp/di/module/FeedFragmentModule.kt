package com.example.tmdbapp.di.module

import androidx.lifecycle.ViewModel
import com.example.tmdbapp.di.util.ViewModelKey
import com.example.tmdbapp.ui.feedFragment.FeedFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class FeedFragmentModule {
    @Binds
    @IntoMap
    @ViewModelKey(FeedFragmentViewModel::class)
    abstract fun bindFeedFragmentModule(mainViewModel: FeedFragmentViewModel): ViewModel
}