package com.example.tmdbapp.di.module

import androidx.lifecycle.ViewModel
import com.example.tmdbapp.di.util.ViewModelKey
import com.example.tmdbapp.ui.splashView.SplashActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SplashActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashActivityViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: SplashActivityViewModel): ViewModel

}