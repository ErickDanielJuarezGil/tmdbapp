package com.example.tmdbapp.di.component

import android.app.Application
import com.example.tmdbapp.base.TMDBApplication
import com.example.tmdbapp.di.module.ApplicationModule
import com.example.tmdbapp.di.module.ContextModule
import com.example.tmdbapp.di.module.ViewBindingModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, ContextModule::class, ApplicationModule::class, ViewBindingModule::class])
interface ApplicationComponent: AndroidInjector<DaggerApplication> {

    fun inject(application: TMDBApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }

}