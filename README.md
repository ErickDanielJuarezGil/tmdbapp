# README #

Project created with the simple end of be an exam to apply to be a Rappier

### What is this repository for? ###

* Container of the TMDB App
* Version 0.1

### How do I get set up? ###

* Download this repo
* Setup into Android Studio
* No key files needed
* Enjoy

### Git rules? ###

* [You should use this conventional commit specification](https://www.conventionalcommits.org/en/v1.0.0/)
* [Learn more about git flow](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=El%20flujo%20de%20trabajo%20de,de%20la%20publicaci%C3%B3n%20del%20proyecto.)